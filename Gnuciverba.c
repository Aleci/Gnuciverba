//CARATTERISTICHE FONDAMENTALI DA AGGIUNGERE:
//AGGIUNGERE AL DIZIONARIO LE PAROLE INSEMINATE ALL'INIZIO PER EVITARE PROBLEMI CON vediSeEsisteO-V
//AGGIORNAMENTO DELLE VARIABILI D'USO DOPO OGNI INSEMINAZIONE (PER LE PAROLE GIA PRESENTI NEL DIZIONARIO)
//4 UNTESTED FUNCIONS h-v~getWord fixDic fillDictionary

//CARATTERISTICHE AGGIUNTIVE:
//STAMPARE A VIDEO CASELLA DI INCEPPO DOPO UN NUMERO DI FALLIMENTI CONSECUTIVI PER RISOLVERE CON UNA NUOVA INSEMINAZIONE
//GENERARE WARNING PRIMA DI SEGFAULT DIZIONARIO INCOMPLETO

//ATTENTION: the dictionary must have at least one word for every lenght in range.
//           There is no protection for an empty group.

//Gnuciverba's settings 
#define ROWS 5      //number of rows of crossword's rectangle
#define COLUMNS 5   //number of columns of crossword's rectangle

#define MAX_LENGHT 10 //max lenght of the words used in the crossword
#define MIN_LENGHT 3  //min lenght of the words used in the crossword (different values not tested)
//======================================================================================================================

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>

struct List{
  char word[MAX_LENGHT+1]; //save data here
  int used; //if the word is already used i will not use it twice (1 used;0 not yet)
  struct List *next;
};

struct Dictionary {
  struct List *group; //the group of word with the same lenght
  int x; //the lenght
  int n; //number of elemet of the group
  struct Dictionary *next;
};

struct Dictionary *dictionary=NULL;
char gnuciverba[ROWS+2][COLUMNS+2];

//function that save a crossword in a file, you can choose if overwrite the file or not
void gnuciOutput(char * file,char * readingOption){
  FILE *fp=fopen(file,readingOption);
  int a,b;
  
  //check if fopen worked
  if(fp==NULL)
    puts("Error: impossible to open the gnuciOutput file");
  else{

    //convert '\0' in '#' (keeping the ones in the last column) and ' ' in '_'
    for(a=1;a<=ROWS;a++)
      for(b=1;b<=COLUMNS;b++)
	if(gnuciverba[a][b]=='\0')
	  gnuciverba[a][b]='#';
	else if(gnuciverba[a][b]==' ')
	  gnuciverba[a][b]='_';

    //print in the file the matrix
    for(a=1;a<=ROWS;a++)
      fprintf(fp,"%s\n",&gnuciverba[a][1]);
    
    fclose(fp);
  }//end fopen else
}

//function that get from seed.txt an initialized gnuciverba matrix
void gnucInput(){
  FILE *fp=fopen("seed.txt","r");
  int counter,c;
  char tempString[COLUMNS+2];
  
  //check if fopen works
  if(fp==NULL)
    puts("Error: impossible to open \'seed.txt\'");
  else{

    //for every line of the crossword
    for(counter=1;counter<=ROWS;counter++){
      //get string with also newline at the end
      fgets(tempString,COLUMNS+2,fp);
      
      //discard last caracter during the copy (i'll replace it in the next block)
      strncpy(&gnuciverba[counter][1],tempString,COLUMNS+1);
    }

    //add end of string around the gnuciverba
    //add the vertical ones
    for(c=1;c<=ROWS;c++){
      gnuciverba[c][0]='\0';
      gnuciverba[c][COLUMNS+1]='\0';
    }
    //add the orizontal ones
    for(c=0;c<COLUMNS+2;c++){
      gnuciverba[0][c]='\0';
      gnuciverba[ROWS+1][c]='\0';
    }
    
    //convert '#' and '\n' in '\0' end '_' in ' '
    for(counter=1;counter<=ROWS;counter++)
      for(c=1;c<=COLUMNS;c++)
	if(gnuciverba[counter][c]=='#' || gnuciverba[counter][c]=='\n')
	  gnuciverba[counter][c]='\0';
	else if(gnuciverba[counter][c]=='_')
	  gnuciverba[counter][c]=' ';
    
    fclose(fp);  
  }//end else fopen
}

//function that prints the gnuciverba matrix without letters
void printEmptyGnuciverba(){
  int a,b;

  //stampo caratteri tabella
  for(a=0;a<ROWS+2;a++){
    for(b=0;b<COLUMNS+2;b++)
      if('\0'==gnuciverba[a][b]){  //se e' un carattere di terminazione
	if(0==a || ROWS+1==a)//se prima riga o ultima
	  putchar('-');
        else if(0==b || COLUMNS+1==b)//se prima o ultima colonna
	  putchar('|');
	else //se carattere di terminazione nel gnuciverba
	  putchar('#');  //lo sostituisco con #
      }
      else
	putchar(' ');
    putchar('\n');
  }
  return;
}


//function that prints the gnuciverba matrix
void printGnuciverba(){
  int a,b;

  //stampo caratteri tabella
  for(a=0;a<ROWS+2;a++){
    for(b=0;b<COLUMNS+2;b++)
      if('\0'==gnuciverba[a][b])  //se e' un carattere di terminazione 
	putchar('#');  //lo sostituisco con #
      else
	putchar(gnuciverba[a][b]);
    putchar('\n');
  }

  return;
}


//function that set to 1 the used variable in the struct List
void use_dic(int group,int word){
  struct Dictionary *tmp=dictionary;
  struct List *temp;
  int counter;
  
  //check if the group exists
  if(group>MAX_LENGHT || group<3)
    printf("Error: the group %d do not exists\n",group);
  else{

    //go to the node of the group
    while(tmp->x!=group)
      tmp=tmp->next;

    //go to the word selected (here i can roll the list if i have less word, like modular arithmetic)
    temp=tmp->group;
    for(counter=0;counter<word;counter++)
      temp=temp->next;

    //set to 1
    temp->used=1;
  }//end else
}

//function that check if a word is already used in the crossword
int is_it_used(int group,int word){
  struct Dictionary *tmp=dictionary;
  struct List *temp;
  int counter,ans=2;
  
  //check if the group exists
  if(group>MAX_LENGHT || group<3)
    printf("Error: the group %d do not exists\n",group);
  else{

    //go to the node of the group
    while(tmp->x!=group)
      tmp=tmp->next;

    //go to the word selected (here i can roll the list if i have less word, like modular arithmetic)
    temp=tmp->group;
    for(counter=0;counter<word;counter++)
      temp=temp->next;
    ans=temp->used;
  } 
  return ans;
}

//function that create the interface to treat the dictionary list of list as a matrix
char * dic(int group,int word){
  struct Dictionary *tmp=dictionary;
  struct List *temp;
  int counter;
  
  //check if the group exists
  if(group>MAX_LENGHT || group<MIN_LENGHT)
    printf("Error: the group %d do not exists\n",group);
  else{

    //go to the node of the group
    while(tmp->x!=group)
      tmp=tmp->next;

    //go to the word selected (here i can roll the list if i have less word, like modular arithmetic)
    temp=tmp->group;
    for(counter=0;counter<word;counter++)
      temp=temp->next;
  }//end else
  
  return temp->word;
}

//function that create the definitive list of groups in the dictionary
void initDictionary(){
  int counter=MIN_LENGHT;
  struct Dictionary *node,*temp;

  //creat all the empty groups
  while(counter<=MAX_LENGHT){

    //create a new node
    node=malloc(sizeof(struct Dictionary));
    if(node==NULL)
      puts("Error: impossible to create a new struct Dictionary node in the heap memory");
    else{//begin malloc else

      //set up the group info
      node->group=NULL;
      node->x=counter;
      node->n=0;
      
      if(dictionary==NULL){//if is empty
	dictionary=node; //inizialize the list
	node->next=node; //when you are alone the person next to you are you...
      }
      else{//if is not empty
	node->next=dictionary;//if you are the last one the next to you will be the first one
	temp=dictionary;
	while(temp->next!=dictionary)//go to the end
	  temp=temp->next;
	temp->next=node;//insert the node as last node
      } 
    }//end malloc else
    counter++;
  }//end of while
}

//function that adds the word at the group with the same lenght
void addWordAtgroup(char *word,int len){
  struct Dictionary *temp=dictionary;
  struct List *tmp;
  
  //go to the correct group
  while(temp->x!=len)
    temp=temp->next;

  //increase number of word in the group
  temp->n++;
  
  //add a new node in the heap
  struct List *node=malloc(sizeof(struct List));
  if(node==NULL)
    puts("Error: impossible to create a new struct List* node in the heap memory");
  else{//begin malloc else

    //save data in the node
    strcpy(node->word,word);
    node->used=0;//the word is not used by default
    
    //add the word at the circular list
    if(temp->group==NULL){//if is an empty group
      temp->group=node; //inizialize the list
      node->next=node; //when you are alone the person next to you are you...
    }
    else{//if is not an empty group
      node->next=temp->group;//if you are the last one the next to you will be the first one
      tmp=temp->group;//recycle var
      while(tmp->next!=node->next)//go to the end
	tmp=tmp->next;
      tmp->next=node;//insert the node as last node
    }
  }//end malloc else
} 

//UNTESTED
void fixDic(){
  FILE * fp=fopen("dictionary.txt~","w");
  if(fp==NULL)
    puts("Error: impossible to open \'dictionary.txt~\'");
  else{
    int a,b;
    struct Dictionary * ptr=dictionary;
    if(ptr!=NULL)
      for(a=MIN_LENGHT;a<=MAX_LENGHT;a++){
	for(b=0;b<ptr->x;b++)
	  fprintf(fp,"%s\n",dic(a,b));
	ptr=ptr->next;
      }
  }//end else fopen
}

//UNTESTED
//function that opens dictionary.txt and fills the dictionary's struct
void fillDictionary(){
  int flag=0; //if 1 dictionary needs fixup

  /*DICTIONARY PROPERTIES
   -All uppercase letters
   -Words lenght between MIN_LENGHT and MAX_LENGHT
   -Just alphabetic characters*/
  
  //open the file
  FILE * fp=fopen("dictionary.txt","r");
  if(fp==NULL)
    puts("Error: impossible to open \'dictionary.txt\'");

  
  else{//begin fopen else
    initDictionary();

    //extract all the words from the file
    while(!feof(fp)){
      
      char word[MAX_LENGHT+25];//string bigger to avoid almost all segmentation faults
      do{
	int badWord=0;
	if(!feof(fp))
	  fscanf(fp,"%s%*c",word);
	if(strlen(word)>MAX_LENGHT || strlen(word)<MIN_LENGHT){
	  flag=1;
	  badWord=1; //again please
	}
      }while(badWord && !feof(fp));

      //recycle for a better world (or word because now could be bad)!
      for(badWord=0;badWord<strlen(word);badWord++){
	if(!isalpha(word[badWord])){//if there is a number
	  badWord=0;
	  break;//no way bro
	}
	if(!isupper(word[badWord]))
	   word[badWord]=toupper(word[badWord]);
      }
	   
      //add the word at the group with the correct lenght
      if(!feof(fp) && badWord)
	addWordAtgroup(word,strlen(word));
      
    }//end of while
  fclose(fp);
  
  if(flag)
    fixDic();
  }//end fopen else
}

//function that prints all the words of the dictionary divided by groups of lenght
void printAllDictionary(){
  struct Dictionary *tmp=dictionary;
  struct List *temp;

  //IN THIS FUNCTION I USE DO...WHILE BECAUSE THE LISTS ARE ROLLING
  //SO THE CONDITIONS WILL BE FALSE AT THE BEGINNING AND AT THE END

  //check if is an empty list
  if(dictionary==NULL)
    puts("\nThe dictionary is empty");
  else{
    
    //for every group of the dictionary
    do{
      printf("\nGroup %d with %d words\n",tmp->x,tmp->n);
      temp=tmp->group;

      //check if is an empty list
      if(temp==NULL)
	puts("This group is empty");
      else{
    
	//for every word of the group
	do{
	  printf("%s ",temp->word);
	  temp=temp->next;
	}while(temp->next!=tmp->group);

	//do while will print this twice if there is just one word in the group
	if(tmp->n!=1)
	  printf("%s",temp->word);
	puts(" ");
      }
      tmp=tmp->next;
    }while(tmp->next!=dictionary);
    
    //repeat last time the while block to avoid to forget last group
    printf("\nGroup %d with %d words\n",tmp->x,tmp->n);
    temp=tmp->group;

    //check if is an empty list
    if(temp==NULL)
      puts("This group is empty");
    else{
    
      //for every word of the group
      do{
	printf("%s ",temp->word);
	temp=temp->next;
      }while(temp->next!=tmp->group);

      //do while will print this twice if there is just one word in the group
      if(tmp->n!=1)
	printf("%s",temp->word);
      puts(" ");
    }
  }
}


//Copy into the gnuciverba the string in horizontal starting from the selected box
//ATTENTION: no check if there is enough space in the gnuciverba matrix
void copiaNelGnuciverbaO(const char * const stringa,const int riga, const int colonna){
  int a;
  
  //per ogni locazione della stringa
  for(a=0;'\0'!=stringa[a];a++)
    //se non scrivo su un carattere di terminazione
    if('\0'==gnuciverba[riga][colonna+a])
      printf("Errore copiaNelGnuciverbaO: tentata scrittura di \'%c\' su carattere di terminazione [%d,%d]\n",
	     stringa[a],riga,colonna+a);
    else
      gnuciverba[riga][colonna+a]=stringa[a];  //copio normalmente

  //copio anche carattere di terminazione (quadratino nero nel gnuciverba finito)
  gnuciverba[riga][colonna+a]='\0'; 
  
  return;
}

//same job of the previous function but doesn't overwrite A-Z caracters
void copiaAsteriscoNelGnuciverbaO(const char * const stringa,const int riga, const int colonna){
  int a;
  
  //per ogni locazione della stringa
  for(a=0;'\0'!=stringa[a];a++)
    //se non scrivo su un carattere di terminazione
    if('\0'==gnuciverba[riga][colonna+a])
      printf("Errore copiaAsteriscoO: tentata scrittura di \'%c\' su carattere di terminazione [%d,%d]\n",
	     stringa[a],riga,colonna+a);
    else if(!isalpha(gnuciverba[riga][colonna+a])) //se non scrivo su una lettera
      gnuciverba[riga][colonna+a]=stringa[a];  //copio normalmente

  //copio anche carattere di terminazione (quadratino nero nel gnuciverba finito)
  gnuciverba[riga][colonna+a]='\0'; 
  
  return;
}

//Copy into the gnuciverba the string in vertical starting from the selected box
//ATTENTION: no check if there is enough space in the gnuciverba matrix
void copiaNelGnuciverbaV(const char * const stringa,const int riga, const int colonna){
  int a;
  
  //per ogni locazione della stringa
  for(a=0;'\0'!=stringa[a];a++)
    //se non scrivo su un carattere di terminazione
    if('\0'==gnuciverba[riga+a][colonna])
      printf("Errore copiaNelGnuciverbaV: tentata scrittura di \'%c\' su carattere di terminazione [%d,%d]\n",
	     stringa[a],riga+a,colonna);
    else
      gnuciverba[riga+a][colonna]=stringa[a];  //copio normalmente

  //copio anche carattere di terminazione (quadratino nero nel gnuciverba finito)
  gnuciverba[riga+a][colonna]='\0'; 
  
  return;
}

//same job of the previous function but doesn't overwrite A-Z caracters
void copiaAsteriscoNelGnuciverbaV(const char * const stringa,const int riga, const int colonna){
  int a;
  
  //per ogni locazione della stringa
  for(a=0;'\0'!=stringa[a];a++)
    //se non scrivo su un carattere di terminazione
    if('\0'==gnuciverba[riga+a][colonna])
      printf("Errore copiaAsteriscoV: tentata scrittura di \'%c\' su carattere di terminazione [%d,%d]\n",
	     stringa[a],riga+a,colonna);
 
    else if(!isalpha(gnuciverba[riga+a][colonna])) //se non scrivo su una lettera
      gnuciverba[riga+a][colonna]=stringa[a];  //copio normalmente

  //copio anche carattere di terminazione (quadratino nero nel gnuciverba finito)
  gnuciverba[riga+a][colonna]='\0'; 
  
  return;
}


//count the number of boxes in vertical between the actual (included) and the first one with the '\0' caracter. 
int contaVerticale(const int riga,const int colonna){
  int a;

  //scorro in verticale finche non trovo il carattere di terminazione
  for(a=riga;'\0'!=gnuciverba[a][colonna];a++);
  
  return a-riga;
}

//count the number of boxes in horizontal between the actual (included) and the first one with the '\0' caracter. 
int contaOrizzontale(const int riga,const int colonna){
  int a;

  //scorro in verticale finche non trovo il carattere di terminazione
  for(a=colonna;'\0'!=gnuciverba[riga][a];a++);
  
  return a-colonna;
}

//draw a word from the group with the correct lenght
char * draWord(const int lenght){
  if(lenght > MAX_LENGHT)
    puts("Error: draWord tried to draw a word too much long");
  else if(lenght < MIN_LENGHT)
    puts("Error: draWord tried to draw a word too much short");
  else{

    struct Dictionary * dPtr;
    struct List * gPtr;
    int n,i;

    //find the group with the correct lenght in the dictionary
    dPtr=dictionary;
    while(dPtr->x != lenght)
      dPtr=dPtr->next;
    gPtr=dPtr->group;

    //draw a number and go to it's binded word
    n=rand()%(dPtr->n);
    for(i=0;i<n;i++)
      gPtr=gPtr->next;

    //return the first not used word from that one
    if(!gPtr->used)
      return gPtr->word;
    else{
      struct List * tmp; //prevent infinite loop
      tmp=gPtr;
      while(gPtr->used && tmp!=gPtr)
	gPtr=gPtr->next;
      if(!gPtr->used)
	return gPtr->word;
    }//end else used
  }//end else error
  return NULL;
}

//draw a number between MIN_LENGHT and maxLenght<=MAX_LENGHT
int drawLenght(int maxLenght){
  int n;
  if(maxLenght < MIN_LENGHT)
    puts("Error: drawLenght tried to draw a lenght too much short");
  if(maxLenght > MAX_LENGHT){
    puts("Warning: drawLenght tried to draw a lenght too much long");
    maxLenght=MAX_LENGHT;
  }
  n=rand()%(maxLenght-MIN_LENGHT+1);
  n+=MIN_LENGHT; //set the correct range
  return n;
}


//draw a casual word with lenght equal or lesser than max
char * word(const int max){
  return draWord(drawLenght(max));
}

void cleanUsed(){
  struct Dictionary * d;
  struct List * l;
  
  //IN THIS FUNCTION I USE DO...WHILE BECAUSE THE LISTS ARE ROLLING
  //SO THE CONDITIONS WILL BE FALSE AT THE BEGINNING AND AT THE END
  
  d=dictionary;
  do{
    l=d->group;
    do{
      l->used=0;
      l=l->next;
    }while(l!=d->group);
    d=d->next;
  }while(d!=dictionary);
}

//reinitialize gnuciverba
void gnuciClean(){
  int a,b;
  
  //inizializzo matrice a spazio e la riquadro con caratteri di terminazione
  for(a=0;a<ROWS+2;a++)//per ogni riga
    for(b=0;b<COLUMNS+2;b++)//per ogni colonna
      if( 0==a || 0==b || ROWS+1==a || COLUMNS+1==b ) //se sto al bordo
	gnuciverba[a][b]='\0';
      else
	gnuciverba[a][b]=' ';

  cleanUsed();
}

//funzione che determina se s1 e' uguale a s2 (asterischi considerati jolly)
//Esempio: strCompara(stringa,dizionario[lun][x]);
int strCompara(const char * const s1,const char * const s2){
  int a,bene;
  bene=1;//se supero tutti i controlli s1==s2

  if(strlen(s1)!=strlen(s2))//se una e' piu lunga dell'altra
    bene=0;//di che stamo a parla

  for(a=0;a<strlen(s1);a++)//considero tutte le locazioni
    if(s1[a]!=s2[a])//se ne becco due diverse
      if('*'!=s1[a])//se non e' un jolly
	bene=0;//so diverse le stringhe
  
  return bene;
}

//funzione che determina se i primi n caratteri di s1 sono uguali a quelli di s2 (asterischi considerati jolly)
//Esempio: strnCompara(stringa,dizionario[lun][x]);
int strnCompara(const char * const s1,const char * const s2,const int n){
  int a,bene;
  bene=1;//se supero tutti i controlli tutto apposto

  for(a=0;a<n;a++)//considero tutte le locazioni
    if(s1[a]!=s2[a])//se ne becco due diverse
      if('*'!=s1[a])//se non e' un jolly
	bene=0;//so diverse le stringhe
  
  return bene;
}

//check if a word is in the dictionary
struct List* dic_search(const char * word){
  int l;
  struct Dictionary * d;
  struct List * ptr,*tmp;
  l=strlen(word);
  d=dictionary;

  if(l<MIN_LENGHT)
    return NULL;
  
  else if(l>MAX_LENGHT)
    return NULL;
  
  else{
    while(d->x != l)
      d=d->next;
    ptr=d->group;
    tmp=ptr;
    do{// rolling dictionary's group
      if(strCompara(word,tmp->word))
	l=0; //recycling is good for the earth
      tmp=tmp->next;
    }while(tmp!=ptr && l);

    if(l)
      return NULL;
    
    return tmp;
  }
}

int search(const char * word){
  return !(dic_search(word)==NULL);
}

//UNTESTED
//get next horizontal word from pos in the gnuciverba
void horizontalGetWord(int * const pos,char * const word){
  //  char * word[COLUMNS];
  char * gnucistring[ROWS*COLUMNS];
  int x,y;
  
  //copy gnuciverba into gnucistring
  for(x=0;x<ROWS;x++)
    for(y=1;y<COLUMNS;y++)//skip first column of #
      gnucistring[x+y]=gnuciverba[x][y];

  while(y){
    //get a word
    for(x=*pos;gnucistring[x]!='\0';x++)
      parola[x-*pos]=gnucistring[x];
    //save new unread position
    *pos=x+1;

    //check if i can return this word
    y=1;
    for(x=strlen(word)-1;x>=0;x--)
      if(!isalpha(word[x]))
	y=0;
  }//end of while
}

//UNTESTED
//get next vertical word from pos in the gnuciverba
void verticalGetWord(int * const pos,char * const word){
  //  char * word[ROWS];
  char * gnucistring[ROWS*COLUMNS];
  int x,y;
  
  //copy gnuciverba into gnucistring
  for(x=0;x<COLUMNS;x++)
    for(y=1;y<ROWS;y++)//skip first row of #
      gnucistring[x+y]=gnuciverba[x][y];

  while(y){
    //get a word
    for(x=*pos;gnucistring[x]!='\0';x++)
      parola[x-*pos]=gnucistring[x];
    //save new unread position
    *pos=x+1;

    //check if i can return this word
    y=1;
    for(x=strlen(word)-1;x>=0;x--)
      if(!isalpha(word[x]))
	y=0;
  }//end of while
}

//extract an horizontal string from gnuciverba and check if exists in the dictionary
int vediSeEsisteO(const int x,int y){
  char appoggio[COLUMNS+1];//piu lunghe non le potrei trovare perche non entrerebbero nella tabella
  int c,trovato;

  //torno indietro a inizio parola qualora non ci fossi gia
  while('\0'!=gnuciverba[x][y-1])
    y--;
    
  //copio la parola in appoggio
  for(c=0;'\0'!=gnuciverba[x][y+c];c++)
    appoggio[c]=gnuciverba[x][y+c];
  appoggio[c]='\0';

  if(strlen(appoggio)<MIN_LENGHT)
    trovato=1;
  else if(search(appoggio))
    trovato=1;
  else
    trovato=0;

  return trovato;
}

//extract a vertical string from gnuciverba and check if exists in the dictionary
int vediSeEsisteV(int x,const int y){
  char appoggio[ROWS+1];//piu lunghe non entrerebbero nel gnuciverba
  int c,trovato;

  //torno indietro a inizio parola qualora non ci fossi gia
  while('\0'!=gnuciverba[x-1][y])
    x--;
  
  //copio la parola in appoggio
  for(c=0;'\0'!=gnuciverba[x+c][y];c++)
    appoggio[c]=gnuciverba[x+c][y];
  appoggio[c]='\0';

  if(strlen(appoggio)<MIN_LENGHT)
    trovato=1;
  else if(search(appoggio))
    trovato=1;
  else
    trovato=0;

  return trovato;
}

//funzione da lanciare prima del cruciCheck per evitari falsi positivi dovuti agli spazi
int cruciCheckSpazi(){
  int bene,a,b;
  bene=1;
  
  for(a=1;a<ROWS+1 && bene;a++)//per ogni riga utile
    for(b=1;b<COLUMNS+1 && bene;b++)//per ogni colonna utile
      if(' '==gnuciverba[a][b])//se trovo uno spazio
	bene=0;//non va bene il gnuciverba
  
  return bene;
}

//funzione che controlla che un gnuciverba senza spazi sia corretto
int gnuciCheck(){
  int bene,a,b;
  bene=1;
  
  for(a=1;a<ROWS+1 && bene;a++){//per ogni riga utile
    for(b=1;b<COLUMNS+1 && bene;b++)//per ogni colonna utile
      if(!(vediSeEsisteV(a,b) && vediSeEsisteO(a,b)))
	bene=0;
      //      putchar('0'+!(vediSeEsisteV(a,b) && vediSeEsisteO(a,b)));
    //    putchar('\n');
  }
  
  return bene;
}

//complete a word
int completeVertical(const int x, const int y){
  char string[ROWS+1]; //same lenght of gnuciverba
  struct Dictionary * d;
  struct List * g;
  int space,found,preexisting,u,c,l,init1,init2,k;
  
  u=x;//save excursion
  found=0;
  preexisting=0;
  
  //go to the begin of the word to complete
  while('\0'!=gnuciverba[u-1][y])
    u--;

  space=contaVerticale(u,y);
 
  if(space<MIN_LENGHT){ //just fill spaces with stars
    found=1;
    while(gnuciverba[u][y]!='\0'){
      if(!isalpha(gnuciverba[u][y]))
	gnuciverba[u][y]='*';
      u++;
    }
  }
 
  if(!found){
    //copy word in string
    for(c=0;gnuciverba[u+c][y]!='\0';c++)
      string[c]=gnuciverba[u+c][y];
    string[c]='\0';

    //fill spaces with stars
    for(c=0;string[c]!='\0';c++)
      if(string[c]==' ')
	string[c]='*';
    
    l=strlen(string);
    
    //if space==l the word is already complete, but only if doesn't have any stars
    if(l==space){
      preexisting=1;
      for(c=0;string[c]!='\0';c++)
	if(string[c]=='*')
	  preexisting=0;
    }

    if(!preexisting){
      //draw a lenght between l and space
      do
	init1=drawLenght(space); 
      while(init1<l); //drawLenght draws a number between MIN_LENGHT and space

      //begin from init1 so go to it
      d=dictionary;
      while(d->x != init1)
	d=d->next;
 
      //for every group
      for(k=0;k<space-MIN_LENGHT && !found;k++){
	d=d->next;

	//select only groups in range (MIN_LENGHT space)
	while(d->x > space)
	  d=d->next;

	//go to init2 to begin from there
	g=d->group;
	for(init2=rand()%d->n;init2!=-1;init2--)
	  g=g->next;

	//compare every word 
	for(c=0;c<d->n && !found;c++){
	  //printf("x=%d, n=%d %s, %p\n",d->x,d->n,g->word,g->next);
	  if(!g->used && strnCompara(string,g->word,d->x))
	    found=1;
	  else
	    g=g->next;
	
	}
      }
    }
    if(found && !preexisting){
      copiaNelGnuciverbaV(g->word,u,y);
      g->used=1;
    }
  }
  return found;
}

//complete a word
int completeHorizontal(const int x, const int y){
  char string[COLUMNS+1]; //same lenght of gnuciverba
  struct Dictionary * d;
  struct List * g;
  int space,found,preexisting,u,c,l,init1,init2,k;
  
  u=y;//save excursion (probably nosense instruction)
  found=0;
  preexisting=0;
  
  //go to the begin of the word to complete
  while('\0'!=gnuciverba[x][u-1])
    u--;

  space=contaOrizzontale(x,u);

  if(space<MIN_LENGHT){ //just fill spaces with stars
    found=1;
    while(gnuciverba[x][u]!='\0'){
      if(!isalpha(gnuciverba[x][u]))
	gnuciverba[x][u]='*';
      u++;
    }
  }
 
  if(!found){
    //copy word in string
    for(c=0;gnuciverba[x][u+c]!='\0';c++)
      string[c]=gnuciverba[x][u+c];
    string[c]='\0';

    //fill spaces with stars
    for(c=0;string[c]!='\0';c++)
      if(string[c]==' ')
	string[c]='*';
    
    l=strlen(string);
    
    //if space==l the word is already complete, but only if doesn't have any stars
    if(l==space){
      preexisting=1;
      for(c=0;string[c]!='\0';c++)
	if(string[c]=='*')
	  preexisting=0;
    }

    if(!preexisting){
      //draw a lenght between l and space
      do
	init1=drawLenght(space); 
      while(init1<l); //drawLenght draws a number between MIN_LENGHT and space

      //begin from init1 so go to it
      d=dictionary;
      while(d->x != init1)
	d=d->next;
 
      //for every group
      for(k=0;k<space-MIN_LENGHT && !found;k++){
	d=d->next;

	//select only groups in range (MIN_LENGHT space)
	while(d->x > space)
	  d=d->next;

	//go to init2 to begin from there
	g=d->group;
	for(init2=rand()%d->n;init2!=-1;init2--)
	  g=g->next;

	//compare every word 
	for(c=0;c<d->n && !found;c++){
	  //printf("x=%d, n=%d %s, %p\n",d->x,d->n,g->word,g->next);
	  if(!g->used && strnCompara(string,g->word,d->x))
	    found=1;
	  else
	    g=g->next;
	
	}
      }
    }
    if(found && !preexisting){
      copiaNelGnuciverbaO(g->word,x,u);
      g->used=1;
    }
  }
  return found;
}

int gnuciFill(){
  int a,b,bene,n,lunO,lunV;
  //per riempimento quadrato trovo lato minore
  n=ROWS>COLUMNS ? COLUMNS+1 : ROWS+1;
  
  //sempre tutto bene all'inizio
  bene=1;

  //per ogni casella del gnuciverba significativa
  for(a=1;a<n && bene;a++)//per ogni riga
    for(b=1;b<n && bene;b++){//per ogni colonna
      //se la casella e' vuota
      if(' '==gnuciverba[a][b]){
	//	printf("Casella %d,%d vuota\t",a,b);//debug
	lunO=contaOrizzontale(a,b);
	lunV=contaVerticale(a,b);
	if(lunO>=lunV){//se ho piu' spazio libero in orizzontale
	  //       printf("Ho piu spazio in orizzontale\t");//debug
	  if(lunO<MIN_LENGHT)//se ho una o due caselle metto un asterisco (magari in quello dopo lunV>lunO)
	    gnuciverba[a][b]='*';
	  else
	    copiaNelGnuciverbaO(word(lunO),a,b);//immetto nuova word
	  bene=completeVertical(a,b);// vedo se posso dare un senso anche nell'altra dimenzione
	}
	else{ //se invece ho piu spazio in verticale
	  //	  printf("Ho piu spazio in verticale\t");//debug
	  if(lunV<MIN_LENGHT){//se ho solo uno o due caselle metto asterischi
	    if(1==lunV)
	      copiaAsteriscoNelGnuciverbaV("*",a,b);
	    else//se invece ci sono due spazi
	      copiaAsteriscoNelGnuciverbaV("**",a,b);
	  }
	  copiaNelGnuciverbaV(word(lunV),a,b);//immetto nuova word
	  bene=completeHorizontal(a,b);// vedo se posso dare un senso anche nell'altra dimenzione
	}
	//       	putchar('\n');//debug
      }

      //se la casella ha una lettera
      else if(isalpha(gnuciverba[a][b])){
	//	printf("Casella %d,%d piena: %s\n",a,b,&gnuciverba[a][b]);//debug
	bene+=completeHorizontal(a,b);
	//	printf("completeHorizontal(%d,%d)=%d\t%s\n",a,b,bene,&gnuciverba[a][b]);
	bene+=completeVertical(a,b);
	//		printf("completeVertical(%d,%d)=%d\n",a,b,bene);
		//	printGnuciverba();
	bene+=completeHorizontal(b,a);
	//	printf("completeHorizontal(%d,%d)=%d\n",b,a,bene);
      	bene+=completeVertical(b,a);
	//		printf("completeVertical(%d,%d)=%d\n",b,a,bene);
	if(5==bene)//bene stava gia a 1 e ci sommo i quattro positivi delle funzioni completaX
	  bene=1;
	else
	  bene=0;
      }

      //se e' un carattere di terminazione skippo
    }

  return bene;
}

int main() {
  time_t inizio;
  double secondi;
  int ore,minuti,a;
  
  //prendo il tempo ad inizio esecuzione
  inizio=clock();
  
  //insemino funzione rand()
  srand(time(NULL));
  
  //stampo ora di inizio esecuzione
  printf("Esecuzione lanciata\n");
//Inizio generazione--------------------------------------------------------

  fillDictionary();
  do{ 
    gnuciClean();
    gnucInput();
    //  printGnuciverba();
    a=gnuciFill();
  }while(!a && !gnuciCheck());
  printGnuciverba();
  
//Fine generazione----------------------------------------------------------
  
  //ricavo e raffino tempo di esecuzione del programma
  secondi=(double)(clock()-inizio)/CLOCKS_PER_SEC;
  minuti=secondi/60;
  secondi-=(double)minuti*60;
  ore=minuti/60;
  minuti-=ore*60;
  printf("Programma terminato con successo\nTempo di esecuzione: %d Ore %d Minuti %.3f Secondi\n\a",ore,minuti,secondi);
  return 0;
}
